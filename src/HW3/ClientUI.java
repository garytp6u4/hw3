/*
* @author 101503010�����l
*/
package HW3;
import java.awt.AWTEvent;
import java.awt.Button;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.List;
import java.awt.Rectangle;
import java.awt.TextField;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;

import java.net.Socket;

import java.util.HashMap;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;

import org.json.JSONObject;

public class ClientUI extends JFrame {
	private JPanel contentPane;
	private TextField Ip = new TextField();
	private TextField Port = new TextField();
	private TextField numberInputField = new TextField();
	private Label IpLabel = new Label();
	private Label portLabel = new Label();
	private Label numberInputLabel = new Label();
	private Button AddButton = new Button();
	private Button Subtractbutton = new Button();
	private Button disconnectButton = new Button();
	private Button connectButton = new Button();
	private List outputList = new java.awt.List();
	private Socket socketToServer=null;//would be used
	private PrintStream out=null;
	private BufferedReader in=null;

	// Construct the frame
	public ClientUI() {
		enableEvents(AWTEvent.WINDOW_EVENT_MASK);
		try {
			initComponent();
			initialFrame();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// Frame initialization
	private void initialFrame(){
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension frameSize = this.getSize();
		if (frameSize.height > screenSize.height) {
			frameSize.height = screenSize.height;
		}
		if (frameSize.width > screenSize.width) {
			frameSize.width = screenSize.width;
		}
		this.setLocation((screenSize.width - frameSize.width) / 2,
				(screenSize.height - frameSize.height) / 2);
		this.setVisible(true);
	}

	// Component initialization
	private void initComponent() throws Exception {
		
		contentPane = (JPanel) this.getContentPane();
		contentPane.setLayout(null);
		this.setSize(new Dimension(600, 420));
		this.setTitle("Socket Client");
		
		outputList.setBounds(new Rectangle(23, 192, 329, 141));
		
		Ip.setBounds(new Rectangle(23, 55, 146, 27));
		Port.setBounds(new Rectangle(204, 55, 148, 27));
		numberInputField.setBounds(new Rectangle(112, 118, 147, 31));				
		
		IpLabel.setText("IP:");
		IpLabel.setBounds(new Rectangle(23, 24, 147, 29));
		portLabel.setText("Port:");
		portLabel.setBounds(new Rectangle(204, 24, 148, 28));
		numberInputLabel.setText("Number:");
		numberInputLabel.setBounds(new Rectangle(102, 88, 144, 24));
		
		connectButton.setLabel("Connect");
		connectButton.setBounds(new Rectangle(375, 24, 179, 31));
		connectButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				connectActionPerformed(e);
			}
		});
		
		disconnectButton.setLabel("Disconnect");
		disconnectButton.setBounds(new Rectangle(375, 88, 179, 31));
		disconnectButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				disconnectActionPerformed(e);
			}
		});
		
		AddButton.setLabel("ADD");
		AddButton.setBounds(new Rectangle(23, 155, 136, 31));
		AddButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addActionPerformed(e);
			}
		});
		
		Subtractbutton.setLabel("Subtract");
		Subtractbutton.setBounds(new Rectangle(216, 155, 136, 31));
		Subtractbutton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				subtractActionPerformed(e);
			}
		});
		
		contentPane.add(numberInputField, null);
		contentPane.add(numberInputLabel, null);
		contentPane.add(Port, null);
		contentPane.add(portLabel, null);
		contentPane.add(Ip, null);
		contentPane.add(IpLabel, null);
		contentPane.add(outputList, null);
		contentPane.add(disconnectButton, null);
		contentPane.add(connectButton, null);
		contentPane.add(AddButton, null);
		contentPane.add(Subtractbutton, null);
		
		
	}
	
	
	//Could be used
	private void showWarningMessageDialog(String message){
		JOptionPane.showMessageDialog(null, message, "Alert", JOptionPane.WARNING_MESSAGE); 
	}

	// Overridden so we can exit when window is closed
	protected void processWindowEvent(WindowEvent e) {
		super.processWindowEvent(e);
		if (e.getID() == WindowEvent.WINDOW_CLOSING) {
			System.exit(0);
		}
	}

	private void addActionPerformed(ActionEvent e) {
		// add code	
		while(socketToServer!=null){
			try{
				Map map = new HashMap();
		        map.put("operators", "add");
		        map.put("number", Integer.valueOf(numberInputField.getText()).intValue());
		        JSONObject objPackage = new JSONObject(map);			
				out.println(objPackage.toString());
				out.flush();
			}
			/*catch(java.io.IOException f){
				showWarningMessageDialog("Disconnect");	
			}*/
			catch(NumberFormatException f){
				showWarningMessageDialog("Please input a integer");
			}
		break;
		}
		while(socketToServer==null){
			showWarningMessageDialog("Disconnect");
			break;
		}
	}
	
	private void subtractActionPerformed(ActionEvent e) {
		// add code
		while(socketToServer!=null){
			try{
				Map map = new HashMap();
		        map.put("operators", "subtract");
		        map.put("number", Integer.valueOf(numberInputField.getText()).intValue());
		        JSONObject objPackage = new JSONObject(map);			
				out.println(objPackage.toString());
				out.flush();
			}
			catch(NumberFormatException f){
				showWarningMessageDialog("Please input a integer");
			}
		break;
		}
		while(socketToServer==null){
			showWarningMessageDialog("Disconnect");
			break;
		}
	}

	private void connectActionPerformed(ActionEvent e) {
		// add code
		while(socketToServer!=null){
			showWarningMessageDialog("Please interrupt the current connection.\nPress End first.");
			break;
		}
		while(socketToServer==null){
			try{
				socketToServer = new Socket(Ip.getText(),
						Integer.valueOf(Port.getText()).intValue());
				out = new PrintStream(socketToServer.getOutputStream());
				in = new BufferedReader(new InputStreamReader(socketToServer.getInputStream()));
				String str = in.readLine();
				outputList.add(str); 
				Thread readerThread = new Thread(new IncomingReader());  
				readerThread.start();
			}
			catch (java.net.UnknownHostException f) {
				showWarningMessageDialog("IP address of the host could not be determined.");
				System.out.println("Error:"+f.getMessage()); 	
			}
			catch(java.lang.IllegalArgumentException f){
				showWarningMessageDialog("port between 0 and 65535.");
				System.out.println("Error:"+f.getMessage());
			}
			catch(java.io.IOException f){
				showWarningMessageDialog("Server is down.");
				System.out.println("Error:"+f.getMessage());
			}
		break;
		}
	}
	
	private void disconnectActionPerformed(ActionEvent e) {
		// add code
		while(socketToServer!=null){
			try{
				out.close();
				in.close();	  	
				socketToServer.close();
			    }
				catch(java.io.IOException f){
					showWarningMessageDialog("Something error.");	
				}
				socketToServer = null;
				out = null;
				in = null;
				outputList.add("disconnected");
			break;
		}
	}
	class IncomingReader implements Runnable{
		  public void run(){
			  try{
				  while(true){
					  String str = in.readLine();
					  JSONObject obj = new JSONObject(str);
					  int inputNum=obj.getInt("result");					  
					  outputList.add(Integer.toString(inputNum)); 
				  }
			  }
			  catch(Exception e ){
				  e.printStackTrace();
			  }  
		  }
	} 
}