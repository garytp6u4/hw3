/*
* @author 101503010�����l
*/
package HW3;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;


public class Exercise3 {
	// Main method
	public static void main(String[] args) {
		
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new ClientUI();
			}
		});
	}
}