/*
* @author 101503010�����l
*/
package HW3;

import java.io.DataInputStream;
import java.io.PrintStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.net.ServerSocket;
import java.util.Iterator;
import java.util.Vector;

import org.json.JSONObject;


public class Server {
	private int listenPort=8888;
	ServerSocket serverSocket = null;
	Socket clientSocket = null;
	int num=0;
	Vector output;
	public static void main(String[] args){
		new Server();
	}

	public Server(){
		output = new Vector();
		try{
		serverSocket = new ServerSocket(listenPort);
		System.out.println("wait to client....");
			while(true){
				clientSocket = serverSocket.accept();
				PrintStream writer = new PrintStream(clientSocket.getOutputStream()); 
				output.add(writer);         
				System.out.println("connect");
				new Thread(new SocketThread(clientSocket)).start();
			}
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public void calculate(int n,int in,String operators){
		if(operators.equals("add")){
			num=n+in;
		}
		else if(operators.equals("subtract")){
			num=n-in;
		}
	}
	
	class SocketThread implements Runnable {
		Socket Client;
		public SocketThread(Socket s) {
			Client = s;
		}
        
		public void update(int num){
				Iterator it = output.iterator();	
				while(it.hasNext()){          
				    try{
				    PrintStream writer = (PrintStream) it.next();  
				    writer.println("{\"result\":\""+num+"\"}"); 
				    //System.out.println("{\"result\":\""+num+"\"}");
				    writer.flush();           
				    }
				    catch(Exception e){
				     System.out.println("error:"+e.getMessage());
				    }
				}
		}
		
	    public void run() {
	    	try {
	    		PrintStream out = new PrintStream(Client.getOutputStream());
	    		DataInputStream in = new DataInputStream(Client.getInputStream());
				out.println(Integer.toString(num));
				out.flush();
				try{
					while(true){
						String inPutData = in.readLine();
						JSONObject obj = new JSONObject(inPutData);
						int str=obj.getInt("number");	
						/*System.out.println("the client comes message :"+inPutData);
						System.out.println("the client comes number :"+str);
						System.out.println("the client comes operators :"+obj.getString("operators"));*/
						calculate(num,str,obj.getString("operators"));
						update(num);						
					}
				}
				catch(EOFException e){
					System.out.println("error:" + e.getMessage());	
				}
				out.close();
				in.close();
				Client.close();
	    	}
	    	catch (IOException e) {
				System.out.println("error:" + e.getMessage());
			}
	    }
	}

}

	
	
